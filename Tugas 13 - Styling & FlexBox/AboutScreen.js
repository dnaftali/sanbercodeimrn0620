import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Image,
    Text
} from 'react-native'

import { MaterialIcons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
export default class App extends Component {

    state = {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }

    render() {

        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <View style={{alignItems:'center'}}>
                        <Text style={[styles.text, styles.label]}>
                            Tentang Saya
                        </Text>
                    </View>
                    <View style={{alignItems:'center'}}>
                        <MaterialIcons name="face" size={120} color="black" />
                    </View>
                    <View style={{alignItems:'center'}}>
                        <Text style={[styles.textName, styles.label]}>
                            Dody Naftali
                        </Text>
                        <Text style={[styles.textProfile, styles.label]}>
                            Newbie React Native Developer
                        </Text>
                    </View>
					<View style={styles.boxPortfolio}>
						<Text>Portfolio:</Text>
                        <View style={{ borderBottomColor: 'black', borderBottomWidth: 1, }}/>
                        <View style={styles.portfolio, {flexDirection: 'row', justifyContent: 'space-between'}}>
                            <View style={{flexDirection: 'column'}}>
                                <MaterialCommunityIcons name="gitlab" size={42} color="#3EC6FF" />
                                <Text style={[styles.label]}>
                                @dnaftali
                                </Text>
                            </View>
                            <View style={{flexDirection: 'column'}}>
                                <MaterialCommunityIcons name="github-circle" size={42} color="#3EC6FF" />
                                <Text style={[styles.label]}>
                                @dnaftali
                                </Text>       
                            </View>
                        </View>
					</View>          
					<View style={styles.boxPortfolio}>
						<Text>Profile:</Text>
                        <View style={{ borderBottomColor: 'black', borderBottomWidth: 1, }}/>
                        <View style={styles.portfolio}>
                            <View style={{flexDirection: 'row'}}>
                                <MaterialCommunityIcons name="facebook" size={32} color="#3EC6FF" />
                                <Text style={[styles.label]}>
                                    dody.naftali
                                </Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <MaterialCommunityIcons name="twitter-box" size={32} color="#3EC6FF" />                          
                                <Text style={[styles.label]}>
                                    @dnaftali
                                </Text>
                            </View>
                            <View style={{flexDirection: 'row'}}>
                                <MaterialCommunityIcons name="instagram" size={32} color="#3EC6FF" />                          
                                <Text style={[styles.label]}>
                                    @dodynaftali
                                </Text>
                            </View>
                        </View>
					</View>                    
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    layout: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.05)',
    },
    box: {
        padding: 25,
        backgroundColor: 'steelblue',
        margin: 5,
    },
    text: {
        fontSize: 36,
        color: '#003366',
        fontWeight: 'bold'
    },
    textName: {
        fontSize: 24,
        color: '#003366',
        fontWeight: 'bold'
    },
    textProfile: {
        fontSize: 18,
        color: '#3EC6FF',
        fontWeight: 'bold',
        fontStyle: "italic"
    },
    boxPortfolio: {
        padding: 10,
        fontSize: 18,
        color: '#3EC6FF',
        fontWeight: 'bold',
        backgroundColor: '#EFEFEF',
        borderRadius:15,
        justifyContent: 'space-around',
        flexDirection: 'column'
    },
    portfolio: {
        flexDirection: 'column',
        margin: 5,
        alignItems:'center'
    },
    label: {
        padding: 4,
        color: '#003366'
    },
    body: {
        paddingTop: 50,
    }
})
// Soal No. 1 (Array to Object)
console.log("Soal No. 1 (Array to Object)")
function arrayToObject(arr) {
    // Code di sini 
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)

    var title = ["firstName","lastName","gender", "age"];
    var peopleObj = {}
    var peopleObjIn = {}
    let fullName
    if (arr.length > 0) {
      for (var i = 0; i < arr.length; i++) {
        var innerArrayLength = arr[i].length;
        fullName = arr[i][0] + " " + arr[i][1]
        var peopleObjIn = {}
        peopleObjIn[title[0]] = arr[i][0]
        peopleObjIn[title[1]] = arr[i][1]
        peopleObjIn[title[2]] = arr[i][2]
        if( typeof arr[i][3] != 'undefined' && Number.isInteger(arr[i][3]) )
        {
          if( arr[i][3] < thisYear  ){
            var age = (thisYear-arr[i][3]);
          }else{
            var age = 'Invalid Birth Year';
          }
        }else{
          var age = 'Invalid Birth Year';
        }
        peopleObjIn[title[2]] = age
        peopleObj[fullName] = peopleObjIn
      }
      console.log(peopleObj)
    } else {
      console.log("")
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

// Soal No. 2 (Shopping Time)
console.log("Soal No. 2 (Shopping Time)")
function shoppingTime(memberId, money) {
  // you can only write your code here!
  let barang = 
  [
    ["Sepatu brand Stacattu", 1500000],
    ["Baju brand Zoro", 500000],
    ["Baju brand H&N", 250000],
    ["Sweater brand Uniklooh", 175000],
    ["Casing Handphone", 50000]
  ]
  let output = {} 
  if (memberId != null && memberId.length > 0) {
    if (money >= 50000 && money != null) {
      output.memberId = memberId
      output.money = money
      let listBarang = []
      let record = 0
      while (record < barang.length) {
        if (money >= barang[record][1]) {
          listBarang.push(barang[record][0])
          money = money - barang[record][1]
        }
        record++
      }
      output.listPurchased = listBarang
      output.changeMoney = money
      return output
    } else {
      return "Mohon maaf, uang tidak cukup"
    }
  } else {
    return "Mohon maaf, toko X hanya berlaku untuk member saja"
  }
  return output
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal No. 3 (Naik Angkot)
console.log("\nSoal No. 3 (Naik Angkot)")
function naikAngkot(arrPenumpang) {
  var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  //your code here
  var rutePoint = [];
  for(let i=0; i<rute.length;i++){
    rutePoint[rute[i]] = i;
  }  
  let detailRute = []
  for (let i = 0; i < arrPenumpang.length; i++) {
    let detail = {}
    detail.penumpang = arrPenumpang[i][0]
    detail.naikDari = arrPenumpang[i][1]
    detail.tujuan = arrPenumpang[i][2]
    let bayar = 0
    for (let j = rutePoint[arrPenumpang[i][1]]; j < rutePoint[arrPenumpang[i][2]]; j++) {
      bayar+=2000
    }
    detail.bayar = bayar
    detailRute[i] = detail
  }
  return detailRute
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]
// If-else 
var nama = "John";
var peran = "Guard";
var pesan = "";

if (nama.length == 0) {
  pesan = "Nama harus diisi!"
} else {
  if (peran.length == 0) {
    pesan += "Halo " + nama + ", Pilih peranmu untuk memulai game!"
  } else {
    pesan += "Selamat datang di Dunia Werewolf, " + nama + "\nHalo " + peran + " " + nama + ", ";
    if (peran.toUpperCase() == "Penyihir".toUpperCase()) {
      pesan += "kamu dapat melihat siapa yang menjadi werewolf!"
    } else if (peran.toUpperCase() == "Guard".toUpperCase()) {
      pesan += "kamu akan membantu melindungi temanmu dari serangan werewolf."
    } else if (peran.toUpperCase() == "Werewolf".toUpperCase()) {
      pesan += "Kamu akan memakan mangsa setiap malam!"
    } else {
      pesan = "Silahkan memilih peran: Penyihir, Guard atau Werewolf"
    }
  }
}
console.log(pesan);

// Switch Case 
var hari = 21; 
var bulan = 1; 
var tahun = 1945;

var text = "";
isValid = true;

switch (true) {
  case (hari>=1 && hari<=31):
    text = hari;
    break;
  default:
    isValid = false;
}

switch (bulan) { //(dengan angka antara 1 - 12)
  case 1:
    text += " " + "Januari";
    break;
  case 2:
    text += " " + "Februari";
    break;
  case 3:
    text += " " + "Maret";
    break;
  case 4:
    text += " " + "April";
    break;
  case 5:
    text += " " + "Mei";
    break;
  case 6:
    text += " " + "Juni";
    break;
  case 7:
    text += " " + "Juli";
    break;
  case 8:
    text += " " + "Agustus";
    break;
  case 9:
    text += " " + "September";
    break;
  case 10:
    text += " " + "Oktober";
    break;
  case 11:
    text += " " + "Nopember";
    break;
  case 12:
    text += " " + "Desember";
    break;
  default:
    isValid = false;
} 

switch (true) {
  case (tahun>=1900 && tahun<=2200):
    text += " " + tahun
    break;
  default:
    isValid = false;
}

switch (isValid) {
  case true:
    console.log(text);
    break;
  default:
    console.log("Tanggal, bulan atau tahun tidak sesuai");
}

// 1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
console.log('1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)');
class Score {
	constructor(subject, points, email) {
      this.subject = subject;
      this.points = points;
      this.email = email;
    }
    average(subject, points, email){
    	let nilai = {
    		email:this.email, 
    		subject:this.subject,
    		points:this.points
    	};
    	if(Array.isArray(this.points)){
    		let total = 0;
			  for (let i = 0; i < this.points.length; i++) {
			    total += this.points[i];
			  }
			nilai.avg = (total/this.points.length);
    	}
    	console.log(nilai);
    }
}
var point = [78, 84, 90, 75];
var score = new Score("Pekan 2", point, "email@sanbercode.com");
score.average();

// 2. SOAL Create Score (10 Poin + 5 Poin ES6)
console.log('2. SOAL Create Score (10 Poin + 5 Poin ES6)');
const data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

const viewScores = (data, subject) => {
  // code kamu di sini
  let title = ["email", "subject", "points"];
  let output = []
  for (let i = 1; i < data.length; i++) {
    let obj = {}
    obj[title[0]] = data[i][0]
    obj[title[1]] = subject
    for (let j = 1; j < data[i].length; j++){
      switch(subject) {
        case "quiz-1":
          obj[title[2]] = data[i][1]
          break;
        case "quiz-2":
          obj[title[2]] = data[i][2]
          break;
        case "quiz-3":
          obj[title[2]] = data[i][3]
          break;
      }
    }
    output[i] = obj
  }
  output.shift();
  console.log(output)
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

// 3. SOAL Recap Score (15 Poin + 5 Poin ES6)
console.log('3. SOAL Recap Score (15 Poin + 5 Poin ES6)');
const recapScores = (data) => {
    // code kamu di sini
    let [judul,...siswa] = data
    for (var i = 0; i < siswa.length; i++) {
      let [email,...points] = siswa[i]
      var nilai = new Score('',points,email).average()
      console.log(`${i+1}. Email: ${email}`)
      console.log(`Rata-rata: ${nilai}`)
      if (nilai > 70) {
        console.log('Predikat: participant')
      } else if(nilai > 80) {
        console.log('Predikat: graduate')
      } else if(nilai > 90) {
        console.log('Predikat: honour')
      }
    }
  }
  
  recapScores(data);
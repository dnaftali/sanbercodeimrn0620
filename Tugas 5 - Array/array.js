// Soal No. 1 (Range)
console.log("Soal No. 1 (Range)");
function range(nParam1, nParam2) {
  var returnArray = [];
  if (nParam1 != null && nParam2 != null){
    if (nParam1 < nParam2) {
      for (var counter = nParam1; counter<=nParam2; counter++){
        returnArray.push(counter);
      }
    } else {
      for (var counter = nParam1; counter>=nParam2; counter--){
        returnArray.push(counter);
      }
    }
    return returnArray
  } else {
    return -1;
  }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal No. 2 (Range with Step)
console.log("\nSoal No. 2 (Range with Step)");
function rangeWithStep(nParam1, nParam2, nStep) {
  var returnArray = [];
  if (nParam1 != null && nParam2 != null && nStep != null){
    if (nParam1 < nParam2) {
      for (var counter = nParam1; counter<=nParam2; counter=counter+nStep){
        returnArray.push(counter);
      }
    } else {
      for (var counter = nParam1; counter>=nParam2; counter=counter-nStep){
        returnArray.push(counter);
      }
    }
    return returnArray
  } else {
    return -1;
  }
}
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal No. 3 (Sum of Range)
console.log("\nSoal No. 3 (Sum of Range)");
function sum(nParam1, nParam2, nStep) {
  if (nStep == null) {
    if(nParam1 != null && nParam2 != null) {
      return range(nParam1, nParam2).reduce(function(a, b){
        return a + b;
      }, 0);
    } else if (nParam1 != null) {
      return nParam1
    } else {
      return 0
    }
  } else {
    return rangeWithStep(nParam1, nParam2, nStep).reduce(function(a, b){
        return a + b;
      }, 0);
  }

}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal No. 4 (Array Multidimensi)
console.log("\nSoal No. 4 (Array Multidimensi)");
function dataHandling(inputArray) {
  var title = ["Nomor ID","Nama Lengkap","TTL", "", "Hobi"];
  var text ="";
  for (var i = 0; i < inputArray.length; i++) {
    var innerArrayLength = inputArray[i].length;
    for (var j = 0; j < innerArrayLength; j++) {
      if(j == 3){
        text = text + " " + inputArray[i][j];
      } else {
        text = text + '\n';
        text = text + title[j] + ' : ' + inputArray[i][j];
      }
    }
    text = text + '\n';
  }
  console.log(text);
}
var input = 
  [
      ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
      ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
      ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
      ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
  ]
dataHandling(input);

// Soal No. 5 (Balik Kata)
console.log("Soal No. 5 (Balik Kata)");
function balikKata(inputText) {
  var arrText = inputText.split("");
  var text = "";
  for (var i = arrText.length; i > 0; i--) {
    text = text + arrText[i-1];
  }
  return text;
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

// Soal No. 6 (Metode Array);
console.log("\nSoal No. 6 (Metode Array)");
function dataHandling2(inputArray) {
  inputArray.splice(1, 4, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro")
  console.log(inputArray);
  var tanggal = inputArray[3].split("/")
  switch (tanggal[1]) { //(dengan string antara 01 - 12)
    case "01":
      console.log("Januari");
      break;
    case "02":
      console.log("Februari");
      break;
    case "03":
      console.log("Maret");
      break;
    case "04":
      console.log("April");
      break;
    case "05":
      console.log("Mei");
      break;
    case "06":
      console.log("Juni");
      break;
    case "07":
      console.log("Juli");
      break;
    case "08":
      console.log("Agustus");
      break;
    case "09":
      console.log("September");
      break;
    case "10":
      console.log("Oktober");
      break;
    case "11":
      console.log("Nopember");
      break;
    case "12":
      console.log("Desember");
      break;
  } 

  tanggal.sort(function(value1, value2){return value2-value1});
  // tanggal.reverse()
  console.log(tanggal)

  tanggal = inputArray[3].split("/").join("-")
  console.log(tanggal)

  var nama = inputArray[1].split("").slice(0,14).join("")
  console.log(nama)
}

var input2 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input2);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 
// 1. Mengubah fungsi menjadi fungsi arrow
console.log("1. Mengubah fungsi menjadi fungsi arrow")
const golden = () => {
    console.log("this is golden!!")
}
golden()

// 2. Sederhanakan menjadi Object literal di ES6
console.log("2. Sederhanakan menjadi Object literal di ES6")
const newFunction = (firstName, lastName) => {
    const fullName = () => {
        console.log(`${firstName} ${lastName}`)
      }
      return {
        firstName,
        lastName,
        fullName
      }
  }
   
//Driver Code 
newFunction("William", "Imoh").fullName() 

// 3. Destructuring
console.log("3. Destructuring")
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
const {firstName, lastName, destination, occupation, spell} = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation)

// 4. Array Spreading
console.log("4. Array Spreading")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

//Driver Code
let combined = [...west, ...east]
console.log(combined)

// 5. Template Literals
console.log("5. Template Literals")
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, ` +  
    `consectetur adipiscing elit, ${planet} do eiusmod tempor ` +
    ` incididunt ut labore et dolore magna aliqua. Ut enim` +
    ` ad minim veniam`
 
// Driver Code
console.log(before) 

// No. 1 Looping While
var bawah = 2;
var atas = 20;
console.log('LOOPING PERTAMA');
var flag = bawah;
while(flag <= atas) {
  console.log(flag + ' I love coding');
  flag = flag + 2;
}
console.log('LOOPING KEDUA');
flag = atas;
while(flag >= bawah) {
  console.log(flag + ' I will become a mobile developer');
  flag = flag - 2;
}

// No. 2 Looping menggunakan for
for(var angka = 1; angka <= 20; angka++) {
  if (angka % 2 == 1){
    if (angka % 3 == 0){
      console.log(angka + ' - I Love Coding');
    } else {
      console.log(angka + ' - Santai');
    }
  } else {
    console.log(angka + ' - Berkualitas');
  }
} 

// No. 3 Membuat Persegi Panjang
var persegiPanjang = "";
var lebar = 1;
do {
  for(var panjang = 1; panjang <= 8; panjang++) {
    persegiPanjang += "#";
  }
  persegiPanjang += "\n";
  lebar++;
}
while (lebar <= 4); 
console.log(persegiPanjang);

// No. 4 Membuat Tangga
var papan = "";
for(var tangga = 1; tangga <= 7; tangga++) {
  for(var anak = 1; anak <= tangga; anak++) {
    papan = papan + "#";
  }
  papan = papan + "\n";
} 
console.log(papan);

// No. 5 Membuat Papan Catur
var vertikal = 0;
var horizontal = 0;
var papanCatur = "";
do {
  while(horizontal <= 8) {
    if (horizontal % 2 == 0){
      papanCatur = papanCatur + " ";
    } else {
      papanCatur = papanCatur + "#";
    }
    horizontal++;
  }
  papanCatur = papanCatur + "\n";
  vertikal++;
  if (vertikal % 2 == 0) {
    horizontal = 0;
  } else {
    horizontal = 1;
  }
  
} while (vertikal < 8);
console.log(papanCatur);
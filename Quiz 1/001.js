// 001.js
function balikString(inputString) {
  // Silakan tulis code kamu di sini
  let text = ""
  let sentences = inputString
  for (var i = inputString.length; i>0; i--) {
    text += sentences[i-1]
  }
  return text
}

function palindrome(inputString) {
  // Silakan tulis code kamu di sini
  let text = ""
  let sentences = inputString
  for (var i = inputString.length; i>0; i--) {
    text += sentences[i-1]
  }
  if (text == inputString){
    return true
  } else {
    return false
  }
}

function bandingkan(num1, num2) {
  // code di sini
  if (num1 < 0 || num2 < 0) {
    return -1
  } else {
    if (num1 > num2) {
      return num1
    } else if (num1 < num2) {
      return num2
    } else {
      return -1
    }
  }
}

// TEST CASES BalikString

console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false

// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18
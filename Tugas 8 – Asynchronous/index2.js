var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]


let i=0
// Lanjutkan code untuk menjalankan function readBooksPromise 
function readBooks(awal,buku) {
    readBooksPromise(awal,buku[i])
        .then(function (sisa) {
            i++
        	readBooks(sisa,buku)
        })
        .catch(function (sisa) {
        });
}

readBooks(10000,books)
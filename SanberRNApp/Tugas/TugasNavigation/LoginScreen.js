import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Image,
    Text,
	TextInput,
    TouchableOpacity,
    Button
} from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialIcons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class App extends Component {

    state = {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }

    render({navigation}) {

        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <View style={{marginTop: 15}}>
                        <Image source={require('./images/logo.png')} style={{height:100}} />
                    </View>
                    <View style={{alignItems:'center', padding: 20}}>
                        <Text style={[styles.textName, styles.label]}>
                            Login
                        </Text>
                    </View>
                    <View style={styles.body}>
                        <View style={styles.form}>
                            <Text style={styles.label}>Username / Email</Text>
                            <TextInput style={styles.input} />
                        </View>
                        <View style={styles.form}>
                            <Text style={styles.label}>Password</Text>
                            <TextInput style={styles.input} />
                        </View>
                        <View style={styles.aksi}>
                            <TouchableOpacity style={styles.btnLight}>
                                <Text style={{color:'white', fontSize:18}}>Sign In</Text>
                            </TouchableOpacity>
                            <Text style={{color:'#3EC6FF', fontSize:20}}>or</Text>
                            <TouchableOpacity style={styles.btnDark} >
                                <Text style={{color:'white', fontSize:18, backgroundColor: '#003366'}}>Register</Text>
                            </TouchableOpacity>
                            <Button
        title="AuthStackScreen"
        onPress={() => navigation.navigate('AuthStackScreen')}
      />
                        </View>
				    </View>                
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    layout: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.05)',
    },
	form: {
		paddingHorizontal: 30,
		marginBottom: 10
	},
    text: {
        fontSize: 36,
        color: '#003366',
        fontWeight: 'bold'
    },
    textName: {
        fontSize: 24,
        color: '#003366',
        fontWeight: 'bold'
    },
	input: {
		padding: 10,
		height: 36,
		borderColor: '#003366',
		borderWidth: 1
	},
    label: {
        padding: 4,
        color: '#003366'
    },
	aksi: {
		marginTop: 30,
		height: 130,
		alignItems: 'center',
		justifyContent: 'space-between',
	},
    btnDark: {
		borderRadius: 16,
		height: 25,
		backgroundColor: '#003366',
		width: 140,
		alignItems: 'center',
		justifyContent: 'center'
	},
    btnLight: {
		borderRadius: 16,
		height: 25,
		backgroundColor: '#3EC6FF',
		width: 140,
		alignItems: 'center',
		justifyContent: 'center'
	},    
    body: {
        paddingTop: 1,
    }
})
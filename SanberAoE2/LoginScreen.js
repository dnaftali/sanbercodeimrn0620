import React from 'react';
import { View, Text, TextInput, StyleSheet, Button, Image, Dimensions } from 'react-native';
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";
import { connect } from 'react-redux'
import { actionCreators } from './todoListRedux'

const DEVICE = Dimensions.get('window');
const mapStateToProps = (state) => ({
  todos: state.todos,
})

class LoginScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isError: false,
    }
  }

  loginHandler() {
    // Kode di sini
    if(this.state.password == 'aaaa'){
      this.onAddTodo(this.state.userName)
      this.props.navigation.push('Home', { userName: this.state.userName });
    } else {
      this.setState({isError: true});
    }
  }

  onAddTodo = (text) => {
    const { dispatch } = this.props
    dispatch(actionCreators.add(text))
  }

  render() {
    return (
      <View style={styles.container}>
        <View>
        <View style={{marginTop: DEVICE.height * 0.01}}>
              <Image source={require('./images/logo.png')} style={styles.itemImage} />
          </View>
          <Text style={styles.subTitleText}>About Age of Empire</Text>
        </View>

        <View style={styles.formContainer}>
          <View style={styles.inputContainer}>
            <MaterialCommunityIcons name='account-circle' color='#8b0000' size={40} />
            <View>
              <TextInput
                style={styles.textInput}
                placeholder=' Username/ E-mail'
                onChangeText={userName => this.setState({ userName })}
              />
            </View>
          </View>

          <View style={styles.inputContainer}>
            <MaterialCommunityIcons name='lock' color='#8b0000' size={40} />
            <View>

              <TextInput
                style={styles.textInput}
                placeholder=' Password'
                onChangeText={password => this.setState({ password })}
                secureTextEntry={true}
              />
            </View>
          </View>
          <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Password Salah (Gunakan password default: aaaa)</Text>
          <Button title='Login' color='#8b0000' onPress={() => this.loginHandler()} />
        </View>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor : '#d19d5e'
  },
  image: {
    flex: 1,
    resizeMode: "stretch",
    justifyContent: "center",
    alignItems: 'center',
  },
  itemImage: {
    justifyContent: 'center',
    width: DEVICE.width * 0.50, height: DEVICE.width * 0.50
  },
  titleText: {
    fontSize: 60,
    fontWeight: 'bold',
    color: '#8b0000',
    textAlign: 'center',
  },
  subTitleText: {
    fontSize: 23,
    fontWeight: 'bold',
    color: '#8b0000',
    alignSelf: 'flex-end',
    marginBottom: 16
  },
  formContainer: {
    justifyContent: 'center'
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 16
  },
  labelText: {
    fontWeight: 'bold'
  },
  textInput: {
    width: 300,
    backgroundColor: 'white'
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  }
});

export default connect(mapStateToProps)(LoginScreen)
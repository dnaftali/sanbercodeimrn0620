import React from 'react';
import { View, Text, FlatList, StyleSheet, ActivityIndicator, Image, Dimensions  } from 'react-native';
import Axios from 'axios';

const DEVICE = Dimensions.get('window')
export default class DetailScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          data: {},
          isLoading: true,
          isError: false,
          selected: {}
        };
    }

      // Mount User Method
  componentDidMount() {
    this.getAPIData()
  }

    //   Get Api Users
    getAPIData = async () => {


        let varApi = this.props.route.params.itemData.apiUri;
        try {
          const response = await Axios.get(varApi)
          let varID = this.props.route.params.itemData.id;
          switch (varID) {
            case "1":
                this.setState({ isError: false, isLoading: false, data: response.data.civilizations })
            break;
            case "2":
                this.setState({ isError: false, isLoading: false, data: response.data.units })
            break;
            case "3":
                this.setState({ isError: false, isLoading: false, data: response.data.structures })
            break;
            case "4":
                this.setState({ isError: false, isLoading: false, data: response.data.technologies })
            break;

        }
        } catch (error) {
          this.setState({ isLoading: false, isError: true })
        }
      }

  render() {
    //  If load data
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    // If data not fetch
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
    // If data finish load
    return (
      <View style={styles.container}>
        <View style={styles.profilContainer}>
          <View >
          <Image source={{ uri: this.props.route.params.itemData.imageUri }} style={styles.itemImage} />
          </View>
          <View style={{borderBottomWidth:5,borderBottomColor:'#8b0000'}}>
          <Text style={{fontSize:28,color:'#003366'}}>{this.props.route.params.itemData.name}</Text>
        </View>
        </View>
        <FlatList 
            data={this.state.data}
            renderItem={({ item }) => {
                let varID = this.props.route.params.itemData.id;
                switch (varID) {
                    case "1":
                        return(
                    
                            <View style={styles.viewList}>
                                <View style={{borderBottomWidth:2,borderBottomColor:'#DDD', paddingBottom:10, paddingTop:10}}>
                                <Text style={styles.textItemLogin}><Text style={styles.profilName}>Name :</Text> {item.name}</Text>
                                <Text><Text style={styles.profilName}>Expansion :</Text> {item.expansion}</Text>
                                <Text><Text style={styles.profilName}>Army Type :</Text> {item.army_type}</Text>
                                <Text><Text style={styles.profilName}>Team Bonus :</Text> {item.team_bonus}</Text>
                                <Text style={styles.profilName}>Civilization Bonus :</Text> 
                                    <Text>{item.civilization_bonus}</Text>
                    
                                </View>
                            </View>
                      )
                        break;
                        case "2":
                            return(
                        
                                <View style={styles.viewList}>
                <View style={{borderBottomWidth:2,borderBottomColor:'#DDD', paddingBottom:10, paddingTop:10}}>
                  <Text style={styles.textItemLogin}><Text style={styles.profilName}>Name :</Text> {item.name}</Text>
                  <Text><Text style={styles.profilName}>Description :</Text> {item.description}</Text>
                  <Text><Text style={styles.profilName}>Expansion :</Text> {item.expansion}</Text>
                  <Text><Text style={styles.profilName}>Age :</Text> {item.age}</Text>
                  <Text><Text style={styles.profilName}>Build Time :</Text> {item.build_time}</Text>
                  <Text><Text style={styles.profilName}>Reload Time :</Text> {item.reload_time}</Text>
                  <Text><Text style={styles.profilName}>Attack Delay :</Text> {item.attack_delay}</Text>
                  <Text><Text style={styles.profilName}>Movement Rate :</Text> {item.movement_rate}</Text>
                  <Text><Text style={styles.profilName}>Line of Sight :</Text> {item.line_of_sight}</Text>
                  <Text><Text style={styles.profilName}>Hit Points :</Text> {item.hit_points}</Text>
                  <Text><Text style={styles.profilName}>Range :</Text> {item.range}</Text>
                  <Text><Text style={styles.profilName}>Attack :</Text> {item.attack}</Text>
                  <Text><Text style={styles.profilName}>Armor :</Text> {item.armor}</Text>
                  <Text><Text style={styles.profilName}>Accuracy :</Text> {item.accuracy}</Text>
                </View>
                                </View>
                          )
                            break;      
                            case "3":
                                return(
                            
                                    <View style={styles.viewList}>
                <View style={{borderBottomWidth:2,borderBottomColor:'#DDD', paddingBottom:10, paddingTop:10}}>
                  <Text style={styles.textItemLogin}><Text style={styles.profilName}>Name :</Text> {item.name}</Text>
                  <Text><Text style={styles.profilName}>Expansion :</Text> {item.expansion}</Text>
                  <Text><Text style={styles.profilName}>Age :</Text> {item.age}</Text>
                  <Text><Text style={styles.profilName}>Build Time :</Text> {item.build_time}</Text>
                  <Text><Text style={styles.profilName}>Line of Sight :</Text> {item.line_of_sight}</Text>
                  <Text><Text style={styles.profilName}>Hit Points :</Text> {item.hit_points}</Text>
                  <Text><Text style={styles.profilName}>Armor :</Text> {item.armor}</Text>
                </View>
                                    </View>
                              )
                                break;          
                                case "4":
                                    return(
                                
                                        <View style={styles.viewList}>
                <View style={{borderBottomWidth:2,borderBottomColor:'#DDD', paddingBottom:10, paddingTop:10}}>
                  <Text style={styles.textItemLogin}><Text style={styles.profilName}>Name :</Text> {item.name}</Text>
                  <Text><Text style={styles.profilName}>Description :</Text> {item.description}</Text>
                  <Text><Text style={styles.profilName}>Expansion :</Text> {item.expansion}</Text>
                  <Text><Text style={styles.profilName}>Age :</Text> {item.age}</Text>
                  <Text><Text style={styles.profilName}>Build Time :</Text> {item.build_time}</Text>
                </View>
                                        </View>
                                  )
                                    break;                                                           
                    default:
                        break;
                }

            }
            }
            keyExtractor={({ id }, index) => index}
          />

      </View>
    );
  }

};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  header: {
    backgroundColor: 'white',
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  profilContainer: {
    flexDirection: 'row',
  },
  profilDetails: {
    paddingHorizontal: 15,
    flex: 1
  },
  profilHai: {
    fontSize: 16
  },
  profilName: {
    flex: 1,
    fontWeight: 'bold',
    color: '#003366'
  },
  boxKategori: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginBottom: 10
  },
  textKategoriContainer: {
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor:'#B4E9FF',
    borderRadius:10
  },
  textKategori: {
    fontWeight: 'bold',
    color:'#003366'
  },
  headerText: {
    color: 'white',
    fontSize: 18,
    padding: 26,
  },
  itemImage: {
    width: DEVICE.width * 0.20, height: DEVICE.width * 0.20
  },
  scrollContainer: {
    flex: 1,
    marginBottom: 100,
  }
});

